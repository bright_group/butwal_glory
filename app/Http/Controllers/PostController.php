<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $post = Post::all();
      return view('layouts/cms/dashboard/post/index',compact('post'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('layouts.cms.dashboard.post.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $post = new Post();

        $post->post_title = $request->input('post_title');
        $post->post_content = $request->input('post_content');
        $post->post_category = $request->input('post_category');
        $post->post_status = $request->input('post_publish');
        $post->save();

   return redirect()->action('postController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {


        $category = Category::all();
        $post = Post::find($post->id);
        return view ('layouts.cms.dashboard.post.edit',compact('post','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post = Post::find($post->id);


        $post->post_title = $request->input('post_title');
        $post->post_content = $request->input('post_content');
        $post->post_category = $request->input('post_category');
        $post->post_status = $request->input('post_publish');

        $post->save();
        return redirect()->action('postController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
         Post::where('id', $post->id)->delete();

        return redirect()->back();
    }
}
