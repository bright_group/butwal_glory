@extends('master.main')
@include('master.meta')
@include('master.header')
@include('master.sidebar')
@include('master.jsfile')
@include('master.footer')
@section('page-content')
    <div class="page-content-wrapper">
        <div class="page-content">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>POST FORM</header>
            </div>



            <div class="card-body " id="bar-parent2">
                <form method="POST" action="{{action('postController@store')}}" enctype="multipart/form-data">

                <div class="col-md-6 col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                        <label for="post_title">Post Title</label>
                        <input type="text" class="form-control" placeholder="Enter ..." name="post_title" id="post_title">
                    </div>
                    <div class="form-group">
                        <label for="post_image">Upload Image</label>
                        <input type="file" class="form-control" placeholder="Enter ..." name="post_image" id="post_image">
                    </div>




                </div>
                <div class="col-md-6 col-sm-6">

                    <div class="form-group">
                        <label >Publish</label>
                        <select name="post_publish" class="form-control">
                            <option value="1">
                               Yes
                            </option>
                            <option value="0">
                                No
                            </option>
                        </select>
                    </div>
                    <!-- select -->

                    <div class="form-group">
                        <label>Post Category</label>
                        <select name="post_category" class="form-control">
                            <option>Select</option>
                            @forelse($category as $data)
                            <option>{{$data->Category_title}}</option>
                                @empty
                            <option>
                                Category not found
                            </option>
                                @endforelse

                        </select>
                    </div>

                </div>

                <div class="col-md-12 col-sm-12">
                    <!-- textarea -->
                    <div class="form-group">
                        <label for="post_content">Textarea</label>
                        <textarea id="article-ckeditor" class="form-control" rows="3" placeholder="Enter ..." name="post_content" id="post_content"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" >Submit</button>
                </div>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                </form>
            </div>

        </div>
    </div>
</div>
        </div>
    </div>

    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
    @endsection