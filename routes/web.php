<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/cms/dashboard/index');
});

//route::post('menu/store','MenuController@store');

route::resource('menu','menuController');
route::resource('category','categoryController');
route::resource('post','postController');
route::resource('page','pageController');
route::resource('profile','profileController');
Route::get('image-crop', 'ProfileController@imageCrop');
//Route::post('image-crop', 'ProfileController@imageCropPost')->name('image-crop');