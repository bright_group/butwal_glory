@extends('master.main')
@include('master.meta')
@include('master.header')
@include('master.sidebar')
@include('master.jsfile')
@include('master.footer')

@section('page-content')
    <div class="page-content-wrapper">
        <div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-topline-red">
                <div class="card-head">
                    <header>Post List</header>
                    <div class="tools">
                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="btn-group">
                                <a href="{{action('postController@create')}}"><button id="addRow1" class="btn btn-info">
                                    Add New <i class="fa fa-plus"></i>
                                </button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="btn-group pull-right">
                                <button class="btn green-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-print"></i> Print </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="example4">
                        <thead>
                        <tr>
                             <th> SN</th>
                            <th> Post Title </th>
                            <th> Post Category </th>

                            <th> Post Content </th>
                            <th> Publish </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody><?php $i=1; ?>
                        @forelse($post as $data)
                            <tr>
                                <td>
                                    {{$i}}

                                </td>
                                <td>
                                    {{$data->post_title}}

                                </td>
                                <td>
                                    {{$data->post_category}}

                                </td>
                                <td>
                                    {{$data->post_content}}

                                </td>
                                <td>
                                <?php if($data->post_status == "1"){
                                    echo "Yes";
                            } else {
                                 echo "No";
                            }?>

                                </td>
                                <td>
                                    <a href="{{action('postController@edit', $data->id)}}">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    </a>
                                    <form id="delete-item{{$data->id}}" action="{{action('postController@destroy', $data->id)}}" method="POST">
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                        {{method_field('DELETE')}}
                                        <button  class="btn btn-danger btn-xs" onclick="if (confirm('Are you sure, You want to delete this ?'))
                                                {
                                                event.preventDefault();
                                                document.getElementById('delete-item{{$data->id}}').submit();
                                                }
                                                else{
                                                event.preventDefault();
                                                }">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </form>


                                </td>
                            </tr>

                           @empty
                               <tr>

                                       Data are not found.

                               </tr>
                            <?php $i++; ?>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        </div>
    </div>
    @endsection