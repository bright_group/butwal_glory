@section('footer-content')
<!-- start footer -->
<div class="page-footer">
    <div class="page-footer-inner"> 2017 &copy; RedStar Hospital Theme By
        <a href="mailto:rtthememaker@gmail.com" target="_top">RT Theme maker</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<!-- end footer -->
@endsection
