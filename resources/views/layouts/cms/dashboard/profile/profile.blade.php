@extends('master.main')
@include('master.meta')
@include('master.header')
@include('master.sidebar')
@include('master.jsfile')
@include('master.footer')

@section('page-content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">User Profile</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index-2.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">UI Elements</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">User Profile</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <div class="card card-topline-aqua">
                        <div class="card-body no-padding height-9">
                            <div class="row">
                                <div class="profile-userpic">
                                    <img src="{{asset('assets/images/dp.png')}}" class="img-responsive" alt=""> </div>
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">Dr. Kiran Patel </div>
                                <div class="profile-usertitle-job"> Gynaecologist</div>
                                   <a href="{{action('ProfileController@imageCrop')}}" ><button type="button" class="btn btn-circle blue btn-sm">Change Profile Pic</button>
                                   </a></div>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Followers</b> <a class="pull-right">1,200</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Following</b> <a class="pull-right">750</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Friends</b> <a class="pull-right">11,172</a>
                                </li>
                            </ul>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">

                                <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                <button type="button" class="btn btn-circle red btn-sm">Message</button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                        </div>
                    </div>
                </div>
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-head card-topline-aqua">
                                    <header>User Activity</header>
                                </div>
                                <div class="card-body no-padding height-9">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel">
                                                    <form>
                                                        <textarea class="form-control p-text-area" rows="4" placeholder="Whats in your mind today?"></textarea>
                                                    </form>
                                                    <footer class="panel-footer">
                                                        <button class="btn btn-post pull-right">Post</button>
                                                        <ul class="nav nav-pills p-option">
                                                            <li>
                                                                <a href="#"><i class="fa fa-user"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-camera"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa  fa-location-arrow"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-meh-o"></i></a>
                                                            </li>
                                                        </ul>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="activity-list">
                                                    <li>
                                                        <div class="avatar">
                                                            <img src="img/user/user1.svg" alt="" />
                                                        </div>
                                                        <div class="activity-desk">
                                                            <h5><a href="#">Rajesh</a> <span>Uploaded 5 new photos</span></h5>
                                                            <p class="text-muted">7 minutes ago near Alaska, USA</p>
                                                            <div class="album">
                                                                <a href="#">
                                                                    <img alt="" src="img/mega-img1.jpg">
                                                                </a>
                                                                <a href="#">
                                                                    <img alt="" src="img/mega-img2.jpg">
                                                                </a>
                                                                <a href="#">
                                                                    <img alt="" src="img/mega-img3.jpg">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="avatar">
                                                            <img src="img/user/user2.svg" alt="" />
                                                        </div>
                                                        <div class="activity-desk">
                                                            <h5><a href="#">Sarah Smith</a> <span>Completed the Sight visit.</span></h5>
                                                            <p class="text-muted">2 minutes ago near Alaska, USA</p>
                                                            <div class="location-map">
                                                                <div id="map-canvas"></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="avatar">
                                                            <img src="img/user/user3.svg" alt="" />
                                                        </div>
                                                        <div class="activity-desk">
                                                            <h5><a href="#">John Doe</a> <span>attended a meeting with</span>
                                                                <a href="#">Lina Smith.</a></h5>
                                                            <p class="text-muted">2 days ago near Alaska, USA</p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="avatar">
                                                            <img src="img/user/user4.svg" alt="" />
                                                        </div>
                                                        <div class="activity-desk">
                                                            <h5><a href="#">Kehn Anderson</a> <span>completed the task “wireframe design” within the dead line</span></h5>
                                                            <p class="text-muted">4 days ago near Alaska, USA</p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="avatar">
                                                            <img src="img/user/user5.svg" alt="" />
                                                        </div>
                                                        <div class="activity-desk">
                                                            <h5><a href="#">Jacob Ryan</a> <span>was absent office due to sickness</span></h5>
                                                            <p class="text-muted">4 days ago near Alaska, USA</p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection
