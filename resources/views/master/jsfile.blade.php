@section('jsfile-content')
    <script type="text/javascript">


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });


        $('#upload').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });


        $('.upload-result').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {


                $.ajax({
                    url: "{{route('profile.store') }}",
                    type: "POST",
                    data: {"image":resp, _token: '{{csrf_token()}}' },
                    success: function (data) {

                        html = '<img src="' + resp + '" />';
                        $("#upload-demo-i").html(html);
                    }
                });
            });
        });


    </script>
    <!-- data tables -->
    <script src="{{asset('assets/js/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/table_data.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/layout.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/chart-js/Chart.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/chart-js/utils.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/chart-js/home-data.js')}}" type="text/javascript"></script>


@endsection