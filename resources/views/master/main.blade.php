<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from radixtouch.in/hospital/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Sep 2017 10:16:31 GMT -->
<head>
    @yield('meta-content')
</head>
<body class="page-header-fixed sidemenu-closed-hidelogo page-container-bg-solid page-content-white page-md">
<div class="page-wrapper">
    @yield('header-content')
    <div class="clearfix"> </div>
    <!-- start page container -->
    <div class="page-container">
        @yield('sidebar-content')
        @yield('page-content')
    </div>
    <!-- end page container -->
      @yield('footer-content')
</div>
@yield('jsfile-content')
</body>

<!-- Mirrored from radixtouch.in/hospital/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Sep 2017 10:18:38 GMT -->
</html>