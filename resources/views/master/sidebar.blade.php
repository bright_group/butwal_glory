@section('sidebar-content')
    <!-- start sidebar menu -->
    <div class="sidebar-container">
        <div class="sidemenu-container navbar-collapse collapse fixed-menu">
            <div id="remove-scroll">
                <ul class="sidemenu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                    <li class="sidebar-toggler-wrapper hide">
                        <div class="sidebar-toggler">
                            <span></span>
                        </div>
                    </li>
                    <li class="sidebar-user-panel">
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="img/dp.svg" class="img-circle user-img-circle" alt="User Image" />
                            </div>
                            <div class="pull-left info">
                                <p> Dr.Kiran Patel</p>
                                <a href="#"><i class="fa fa-circle user-online"></i><span class="txtOnline"> Online</span></a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link nav-toggle ">
                            <i class="fa fa-tachometer"></i>
                            <span class="title">Dashboard</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="" class="nav-link ">
                                    <span class="title">Home</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{action('menuController@index')}}" class="nav-link ">
                                    <span class="title">Add Menu</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="email_compose.html" class="nav-link ">
                                    <span class="title">Nevigation</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{action('categoryController@index')}}" class="nav-link ">
                                    <span class="title">Categories</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link nav-toggle">
                            <i class="fa fa-envelope"></i>
                            <span class="title">Events/News</span>
                            <span class="arrow"></span>
                            <span class="label label-rouded label-menu green-bgcolor">3</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="email_inbox.html" class="nav-link ">
                                    <span class="title">Add New Events/News</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="email_view.html" class="nav-link ">
                                    <span class="title">All Events/News</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link nav-toggle"><i class="fa fa-book"></i>
                            <span class="title">Post</span><span class="arrow"></span></a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="{{action('postController@index')}}" class="nav-link ">
                                    <span class="title">Add New Post</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="book_appointment.html" class="nav-link ">
                                    <span class="title">All Post</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle"> <i class="fa fa-user-md"></i>
                            <span class="title">Pages</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="{{action('pageController@index')}}" class="nav-link ">
                                    <span class="title">Add New Page</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="add_doctor.html" class="nav-link ">
                                    <span class="title">All Pages</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle"> <i class="fa fa-users"></i>
                            <span class="title">Downloads</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="all_staffs.html" class="nav-link ">
                                    <span class="title">Add New Download</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="add_staff.html" class="nav-link ">
                                    <span class="title">All Downloads</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="edit_staff.html" class="nav-link "> <span class="title">Edit Staff</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle"> <i class="fa fa-user"></i>
                            <span class="title">Users</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="all_patients.html" class="nav-link ">
                                    <span class="title">Add New User</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="add_patient.html" class="nav-link ">
                                    <span class="title">All users</span>
                                </a>
                            </li>


                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle"> <i class="fa fa-bed"></i>
                            <span class="title">Media</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="room_allotment.html" class="nav-link ">
                                    <span class="title">Add New Media</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="add_room_allotment.html" class="nav-link ">
                                    <span class="title">All Media</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle"> <i class="fa fa-money"></i>
                            <span class="title">Setting</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="payments.html" class="nav-link ">
                                    <span class="title">Header</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="add_payment.html" class="nav-link ">
                                    <span class="title">Miscellaneous Setting </span>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a href="invoice_payment.html" class="nav-link ">
                                    <span class="title">SEO Setup</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="widget.html" class="nav-link">

                                    <span class="title">Widget</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="heading">
                        <h3 class="uppercase">Extra Components</h3>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle">
                            <i class="fa fa-window-restore"></i>
                            <span class="title">Album</span>
                            <span class="label label-rouded label-menu">10</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="ui_buttons.html" class="nav-link ">
                                    <span class="title">Add New Album</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="ui_tabs_accordions_navs.html" class="nav-link ">
                                    <span class="title">All Album</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-id-card-o"></i>
                            <span class="title">Slider </span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="layouts_form.html" class="nav-link ">
                                    <span class="title">Add New Slider</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="advance_form.html" class="nav-link ">
                                    <span class="title">All Slider</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-th-list"></i>
                            <span class="title">Contact Info</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="basic_table.html" class="nav-link ">
                                    <span class="title">All Emails</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="advanced_table.html" class="nav-link ">
                                    <span class="title">Contact Lists</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="#" class="nav-link nav-toggle"> <i class="fa fa-laptop"></i>
                            <span class="title">Contact Form</span> <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="layout_boxed.html" class="nav-link ">
                                    <span class="title">Contact Form</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">Charts</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="charts_echarts.html" class="nav-link ">
                                    <span class="title">eCharts</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="charts_morris.html" class="nav-link ">
                                    <span class="title">Morris Charts</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="charts_chartjs.html" class="nav-link ">
                                    <span class="title">Chartjs</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-map-o"></i>
                            <span class="title">Maps</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="google_maps.html" class="nav-link ">
                                    <span class="title">Google Maps</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="vector_maps.html" class="nav-link ">
                                    <span class="title">Vector Maps</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-file-text"></i>
                            <span class="title">Extra pages</span>
                            <span class="label label-rouded label-menu purple-bgcolor">7</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="login.html" class="nav-link "> <span class="title">Login</span>
                                </a>
                            </li>
                            <li class="nav-item  "><a href="user_profile.html" class="nav-link "><span
                                            class="title">Profile</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="contact.html" class="nav-link "> <span class="title">Contact Us</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="lock_screen.html" class="nav-link "> <span class="title">Lock Screen</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="page-404.html" class="nav-link "> <span class="title">404 Page</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="page-500.html" class="nav-link "> <span class="title">500 Page</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="blank_page.html" class="nav-link "> <span class="title">Blank Page</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-caret-square-o-right"></i>
                            <span class="title">Multi Level Menu</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-university"></i> Item 1
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <i class="fa fa-bell-o"></i> Arrow Toggle
                                            <span class="arrow "></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item">
                                                <a href="javascript:;" class="nav-link">
                                                    <i class="fa fa-calculator"></i> Sample Link 1</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="fa fa-clone"></i> Sample Link 2</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="fa fa-cogs"></i> Sample Link 3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-file-pdf-o"></i> Sample Link 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-rss"></i> Sample Link 2</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-hdd-o"></i> Sample Link 3</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-gavel"></i> Arrow Toggle
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-paper-plane"></i> Sample Link 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-power-off"></i> Sample Link 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-recycle"></i> Sample Link 1
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="fa fa-volume-up"></i> Item 3 </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end sidebar menu -->
@endsection