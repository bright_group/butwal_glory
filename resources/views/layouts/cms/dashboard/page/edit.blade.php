@extends('master.main')
@include('master.meta')
@include('master.header')
@include('master.sidebar')
@include('master.jsfile')
@include('master.footer')
@section('page-content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card card-topline-aqua">
                        <div class="card-head">
                            <header>Page FORM</header>
                        </div>



                        <div class="card-body " id="bar-parent2">
                            <form method="POST" action="{{action('pageController@update', $page->id)}}" enctype="multipart/form-data">

                                {{method_field('PATCH')}}
                                {{csrf_field()}}

                                <div class="col-md-6 col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label for="page_title">page Title</label>
                                        <input type="text" class="form-control" placeholder="Enter ..." value="{{$page->page_title}}" name="page_title" id="page_title">
                                    </div>
                                    <div class="form-group">
                                        <label for="page_image">Upload Image</label>
                                        <input type="file" class="form-control" placeholder="Enter ..." value="{{$page->page_image}}" name="page_image" id="page_image">
                                    </div>




                                </div>
                                <div class="col-md-6 col-sm-6">

                                    <div class="form-group">
                                        <label >Publish</label>
                                        <select  name="page_publish" class="form-control">
                                            <option value="1" <?php  if($page->page_status == "1"){echo 'selected';} ?>>
                                                Yes
                                            </option>
                                            <option value="0" <?php if($page->page_status == "0"){echo 'selected';}?>>
                                                No
                                            </option>
                                        </select>
                                    </div>
                                    <!-- select -->



                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <!-- textarea -->
                                    <div class="form-group">
                                        <label for="page_content">Textarea</label>
                                        <textarea id="article-ckeditor" class="form-control" rows="3" placeholder="Enter ..." name="page_content" id="page_content">{{$page->page_content}}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary" >Submit</button>
                                </div>
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
@endsection