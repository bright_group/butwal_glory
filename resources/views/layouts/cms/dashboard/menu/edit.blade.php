@extends('master.main')
@include('master.meta')
@include('master.header')
@include('master.sidebar')
@include('master.jsfile')
@include('master.footer')

@section('page-content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Form Layouts</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index-2.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li><a class="parent-item" href="#">Forms</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">Form Layouts</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card card-topline-yellow">
                        <div class="card-head">
                            <header>Menu Form</header>
                        </div>
                        <div class="card-body " id="bar-parent">
                            <form method="POST" action="{{action('menuController@update',$menu["id"])}}">
                                {{ method_field('PATCH')}}
                                <div class="form-group">
                                    <label for="Menu">Menu Name</label>
                                    <input type="text" class="form-control" value="{{$menu->menu_name}}" id="Menu" placeholder="please enter the menu" name="menu">
                                </div>
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

               {{-- <div class="col-md-12">
                    <div class="card card-topline-aqua">
                        <div class="card-head">
                            <header>Menu List</header>

                        </div>
                        <div class="card-body ">
                            <table id="example1" class="table table-striped table-bordered table-hover " style="width:100%;">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Menu Name</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(count($name)>0)
                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($name as $data)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$data->menu_name}}</td>
                                            <td>
                                                <a href="/menu/{{$data->id}}/edit"><button class="btn btn-success btn-xs">
                                                        <i class="fa fa-check"></i>
                                                    </button></a>
                                                <button class="btn btn-primary btn-xs">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                                <button class="btn btn-danger btn-xs">
                                                    <i class="fa fa-trash-o "></i>
                                                </button></td>

                                        </tr>
                                        @php $i++; @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <p>Data are not Found</p>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>--}}
            </div>





        </div>
    </div>
@endsection