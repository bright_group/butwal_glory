@extends('master.main')
@include('master.meta')
@include('master.header')
@include('master.sidebar')
@include('master.jsfile')
@include('master.footer')

@section('page-content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Form Layouts</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index-2.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li><a class="parent-item" href="#">Forms</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">Form Layouts</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card card-topline-yellow">
                        <div class="card-head">
                            <header>Category</header>
                        </div>
                        <div class="card-body " id="bar-parent">
                            <form method="POST" action="{{action('categoryController@store')}}" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="category">Category Name</label>
                                    <input type="text" class="form-control" id="category" placeholder="please enter the category" name="category">
                                </div>
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card card-topline-aqua">
                        <div class="card-head">
                            <header>Category List</header>

                        </div>
                        <div class="card-body ">
                            <table id="example1" class="table table-striped table-bordered table-hover " style="width:100%;">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Category Name</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(count($category)>0)
                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($category as $data)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$data->Category_title}}</td>
                                            <td>

                                                <a href="{{action('categoryController@edit',$data->id)}}"> <button class="btn btn-primary btn-xs">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                </a>
                                                <form id="delete-item{{$data->id}}" action="{{action('categoryController@destroy',$data->id)}}" method="POST" enctype="multipart/form-data" >
                                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                    {{method_field('DELETE')}}


                                                    <button class="btn btn-danger btn-xs" type="submit" onclick="if (confirm('Are you sure, You want to delete this ?'))
                                                            {
                                                            event.preventDefault();
                                                            document.getElementById('delete-item{{$data->id}}').submit();
                                                            }
                                                            else{
                                                            event.preventDefault();
                                                            }">
                                                        <i class="fa fa-trash-o "></i>
                                                    </button>
                                                </form>

                                            </td>

                                        </tr>
                                        @php $i++; @endphp
                                    @endforeach
                                @else
                                    <tr>
                                       <td>Data are not Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>
@endsection