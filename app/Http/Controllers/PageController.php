<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $page = Page::all();
return view('layouts.cms.dashboard.page.index',compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.cms.dashboard.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Page = new Page();

        $Page->page_title = $request->input('page_title');
        $Page->page_content = $request->input('page_content');
        $Page->page_image = $request->input('page_image');
        $Page->page_status = $request->input('page_publish');
        $Page->save();
        
        return redirect()->action('pageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $page = page::find($page->id);
        return view ('layouts.cms.dashboard.page.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page = page::find($page->id);


        $page->page_title = $request->input('page_title');
        $page->page_content = $request->input('page_content');
        //$page->page_category = $request->input('page_category');
        $page->page_status = $request->input('page_publish');

        $page->save();
        return redirect()->action('pageController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        page::where('id', $page->id)->delete();

        return redirect()->back();
    }
}
